from sqlalchemy import Column, Integer, String, ForeignKey ,Enum,UniqueConstraint,DateTime, true, Time,Date
from sqlalchemy.orm import relationship
from database import Base
import datetime 



class User(Base):
    
    __tablename__ =  "users"
    id  =  Column(Integer, primary_key  = True  ,index =  True)
    password  =  Column(String(50))
    email  =  Column(String(50))
    name =  Column(String(50))
    time  =    Column(Time(timezone=true))
    usercon =  relationship('GroupMembers',back_populates = "usercon" )
    socketcon =  relationship('UserSocket', back_populates = "usercon")
    __table_args__ = (UniqueConstraint('name', name='user_uc'),)

class UserSocket(Base):
    __tablename__ = "sockets"
    id  =  Column(Integer, primary_key  = True  ,index =  True)
    socketid  =  Column(String(50))
    userid  =  Column(Integer , ForeignKey("users.id"))
    usercon =  relationship('User',back_populates  =  "socketcon" )

class Message(Base):
    __tablename__= 'messages'
    id  =  Column(Integer, primary_key  = True  ,index =  True)
    to_user =   Column(Integer , ForeignKey("users.id"))
    from_user  =    Column(Integer , ForeignKey("users.id"))
    message  =  Column(String(50))
    
    
    
    
class Group(Base):
    __tablename__ = "groups"
    id  =  Column(Integer, primary_key  = True  ,index =  True)
    creation_time  =Column(Time(timezone=true), default=datetime.datetime.now())
    groupname  =  Column(String(50))
    groupmembercon =  relationship('GroupMembers',back_populates = "groupcon" )
    creatorid  =  Column(Integer , ForeignKey("users.id"))

class  GroupMembers(Base):
    
    __tablename__ = "groupmembers"
    id  =  Column(Integer, primary_key  = True  ,index =  True)
   
    groupid =   Column(Integer , ForeignKey("groups.id"))
    userid  =  Column(Integer , ForeignKey("users.id"))
    usercon =  relationship('User'  , back_populates  = "usercon" )
    groupcon  =  relationship('Group', back_populates = "groupmembercon" )

    
    
    
    
