import models
from sqlalchemy import and_
from typing  import List
class User :
    def __init__(self) -> None:
        self.user = {}
    def usersocket(self, socketid:str, userid:int, db):
        usersocket  = models.UserSocket(socketid = socketid ,userid  =  userid)
        db.add(usersocket)
        db.commit()
    def verifyuser(self, username , password , db ):
        user  = db.query(models.User).filter(and_(models.User.name == username , models.User.password ==password) ).first()
      
        return user.id if user  else False
    def existinguser( self,username ,db  ):
        
        # print(username , db)
        user  = db.query(models.User).filter(models.User.name == username  ).first()
        
        return True if user  else False
    def createnewuser(self, username , password , email, db):
        user  =  models.User(name =  username , password =  password, email = email)
        db.add(user)
        db.commit()
        db.refresh(user)
        return user.id
    def SocketConn(self, userid, socketid ,db):
        sockuser =  models.UserSocket(socketid =   socketid , userid = userid)
        db.add(sockuser)
        db.commit() 
    def getsocketid(self,userid ,db ):
        socket = db.query(models.UserSocket).filter(models.UserSocket.userid == userid).first()
        return socket.socketid
    
       
    def updateSocketConn(self, userid , socketid, db):
        data=  {'socketid':socketid, 'userid':userid}
        db.query(models.UserSocket).filter(models.UserSocket.userid == userid).update(data)
        db.commit()
    
    def getuserid(self, username, db):
        user  =  db.query(models.User).filter(models.User.name == username).first()
        return user.id 
    def storemessage(self , message , to_userid , from_userid, db):
        message  =  models.Message(to_user = to_userid, from_user  =  from_userid, message =  message)
        db.add(message)
        db.commit()
    def creategroup(self,  group_name  ,creatorid , db ):
        
        group  = models.Group( groupname = group_name, creatorid  = creatorid )
        
        db.add(group)
        db.commit()
        db.refresh(group)
        return group.id
    def allgroupuser(self ,userid,  db):
        db.query(models.GroupMembers).filter(models.GroupMembers.userid ==userid).all()
    
    
    def adduseringroup(self , group_id , userids:List, db ):
        data  = []
        for username in userids:
            userid  = self.getuserid(username, db)
            data.append(models.GroupMembers(groupid =  group_id , userid =userid  ))
        print(data)
        db.bulk_save_objects(data)
        db.commit()
            
    def alluser(self, db)->List:
        message  = db.query(models.User).all()
        return message
    
    def groupmember(self ,  group_id, db):
        group  =  db.query(models.GroupMembers).filter(models.GroupMembers.groupid==group_id).all()
        return group
        
    
    
    
    # async def userexistinsock(self, userid ):
        # user  = db.query(models.User).filter(models.User.name == username  ).first()

        
        
        
        
        
        
    