
from requests import session
import socketio
from fastapi import FastAPI, Depends
from query import User
import json
from database import get_db, engine
import models
from sqlalchemy.orm import Session
from threadfunc import ThreadFucn

models.Base.metadata.create_all(engine)

sio = socketio.AsyncServer(cors_allowed_origins='*', async_mode='asgi')
app = FastAPI()
socketio_app = socketio.ASGIApp(sio, app)
login_query = User()


db = get_db()


@sio.on("connect")
def connect(sid, environ):
    print("connect ", sid)


@sio.on("signin")
async def login(sid, environ):
    sign_data = json.loads(environ)

    username = sign_data['username']
    password = sign_data['password']

    # Db query for solving that if the user and password has enter perfect passowrd or not
    userid = login_query.verifyuser(username, password, next(get_db()))
    if userid:
        message = "verified"
        login_query.updateSocketConn(userid, sid, next(get_db()))

    else:
        message = "not verified"
    await sio.emit('signin', message, room=sid)


@sio.on("chat")
async def chat(sid,  environ):
    print(type(sid))
    chat_data = json.loads(environ)
    from_username = chat_data['fromname']
    to_username = chat_data['tousername']
    message = chat_data['message']
    to_userid = 0
    from_userid = 0

    to_userid = login_query.getuserid(to_username, next(get_db()))
    from_userid = login_query.getuserid(from_username, next(get_db()))

    to_user_socketid = login_query.getsocketid(to_userid, next(get_db()))

    login_query.storemessage(message, to_userid, from_userid, next(get_db()))

    await sio.emit("chat", json.dumps({'message': message, 'username': from_username}), room=to_user_socketid)


@sio.on('creategroup')
async def creategroup(sid,  environ):
    # print(login_query.allgroupuser(next(get_db())), 'wefew')
    data = json.loads(environ)
    creatorid  = data['creatorid']
    creatorname  = data['creatorname']
    groupname = data['groupname']
    userlist = data['userlist']
    userlist.append(creatorname)
    groupid = login_query.creategroup(
    group_name=groupname, creatorid=creatorid, db=next(get_db()))
    login_query.adduseringroup(groupid, userlist, next(get_db()))

    await sio.emit("creategroup", groupid)

@sio.on('groupchat')
async def groupchat(sid, environ):
    
    data  =  json.loads(environ)
    # clientid  =  data['clientid']
    groupid  =  data['groupid']
    message = data['message']
    useridingroup = login_query.groupmember(group_id=groupid, db  =  next(get_db()))
    
    for user in useridingroup:
        socketid  = login_query.getsocketid(user.userid, next(get_db()))
        await sio.emit("groupchat" ,message, room  = socketid  )
    
    


@sio.on('getalluser')
async def alluser(sid, environ):
    all_user = login_query.alluser(next(get_db()))

    name = []
    for user in all_user:
        name.append(user.name)
    await sio.emit("alluser", name,  room=sid)


@sio.on("signup")
async def signup(sid, environ):
    thread = ThreadFucn()
    sign_data = json.loads(environ)

    # DB query for solving if the similar user has account than the error
    if not login_query.existinguser(sign_data['username'], next(get_db())):

        message = 'new user sign in'
        userid = login_query.createnewuser(
            sign_data['username'], sign_data['password'], sign_data['email'], next(get_db()))
        thread.startthread((userid, sid, next(get_db())),
                           login_query.SocketConn)

    else:
        message = 'not new user'
    await sio.emit('signup', message, room=sid)
    # ThreadFucn.jointhread()




