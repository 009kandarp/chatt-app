from sqlalchemy import create_engine 
from sqlalchemy.ext.declarative import declarative_base 
from sqlalchemy.orm import sessionmaker 

# Location of the database file
SQLALCHEMY_DATABASE_URL = 'sqlite:///./au_backend.db'

# connecting to the engine of the database 
engine= create_engine('mysql+pymysql://root:123456@localhost/chatapp')    


# It will create local session  for the engine 
SessionLocal =  sessionmaker(bind  = engine , autocommit=False,autoflush=False)


Base  =  declarative_base()   

# Getting the database 
def get_db():
    db = SessionLocal()
    try:
        # Generator function for using continous db connection 
        yield db
    finally:
        db.close()