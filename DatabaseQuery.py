import models  
from typing import  List

class DatabaseQuery:
    def __init__(self) -> None:
        pass
    async def all_User(self, db )->List :
        return await db.query(models.User).all()
    async def User(self,db, clientname:str):
        return await db.query(models.User).filter(models.User.name == clientname).first()